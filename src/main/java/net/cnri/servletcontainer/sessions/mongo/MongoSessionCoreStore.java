package net.cnri.servletcontainer.sessions.mongo;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.IndexOptions;
import net.cnri.servletcontainer.sessions.HttpSessionCore;
import net.cnri.servletcontainer.sessions.HttpSessionCoreStore;
import net.cnri.servletcontainer.sessions.HttpSessionManager;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class MongoSessionCoreStore implements HttpSessionCoreStore {
    private static long MAX_TIME_MS = 30_000;
    private static long EXPIRATION_LAG_MS = 600_000;

    private HttpSessionManager sessionManager;
    private MongoClient client;
    private MongoDatabase db;
    private MongoCollection<Document> collection;

    protected abstract String getDatabaseName();
    protected String getCollectionName() {
        return "sessions";
    }
    protected abstract String getConnectionUri(HttpSessionManager sessionManagerParam, String sessionCoreStoreInit);
    protected abstract Object serialize(String key, Object value);
    protected abstract Object deserialize(String key, Object value);

    @Override
    public void init(@SuppressWarnings("hiding") HttpSessionManager sessionManager, String sessionCoreStoreInit) {
        this.sessionManager = sessionManager;
        this.client = new MongoClient(new MongoClientURI(getConnectionUri(sessionManager, sessionCoreStoreInit)));
        this.db = this.client.getDatabase(getDatabaseName());
        this.collection = db.getCollection(getCollectionName());
        {
            IndexOptions indexOptions = new IndexOptions();
            indexOptions.unique(true);
            this.collection.createIndex(new Document("id", 1), indexOptions);
        }
        {
            IndexOptions indexOptions = new IndexOptions();
            indexOptions.expireAfter(0L, TimeUnit.SECONDS);
            this.collection.createIndex(new Document("expiration", 1), indexOptions);
        }
    }

    @Override
    public void clear() {
        collection.deleteMany(new Document());
    }

    @Override
    public void destroy() {
        client.close();
    }

    @Override
    public HttpSessionCore getSession(String id) {
        Document doc = collection.find(new Document("id", id)).maxTime(MAX_TIME_MS, TimeUnit.MILLISECONDS).first();
        if (doc == null) return null;
        return new MongoSessionCore(doc);
    }

    @Override
    public HttpSessionCore createSession(Map<String, Object> attributes) {
        String id = sessionManager.mintNewSessionId();
        long creationTime = System.currentTimeMillis();
        long lastAccessedTime = creationTime;
        int maxInactiveInterval = sessionManager.getSessionTimeoutSeconds();
        Document document = new Document();
        document.put("id", id);
        document.put("creationTime", creationTime);
        document.put("lastAccessedTime", lastAccessedTime);
        document.put("maxInactiveInterval", maxInactiveInterval);
        document.put("expiration", new Date(lastAccessedTime + maxInactiveInterval*1000L + EXPIRATION_LAG_MS));
        document.put("requestCount", 0);
        document.put("valid", true);
        Document attsDoc = new Document();
        if (attributes != null) {
            for (Map.Entry<String, Object> att : attributes.entrySet()) {
                attsDoc.put(encodeKey(att.getKey()), serialize(att.getKey(), att.getValue()));
            }
        }
        document.put("atts", attsDoc);
        collection.insertOne(document);
        return new MongoSessionCore(document);
    }

    @Override
    public Stream<HttpSessionCore> getSessionsByKeyValue(String key, String value) {
        String attsKey = "atts." + encodeKey(key);
        Document searchDoc = new Document(attsKey, serialize(key, value));
        MongoIterable<MongoSessionCore> sessions = collection.find(searchDoc).map(MongoSessionCore::new);
        return sessions.into(new ArrayList<HttpSessionCore>()).stream();
    }

    public static String encodeKey(String key) {
        if (key.equals("_id")) {
            return "%5Fid";
        }
        String res = key.replace("%", "%25");
        res = res.replace(".", "%2E");
        res = res.replace("\u0000", "%00");
        if (res.startsWith("$")) {
            res = res.replaceFirst("^\\$", "%24");
        }
        return res;
    }

    public static String decodeKey(String key) {
        try {
            return URLDecoder.decode(key, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }

    @Override
    public void changeSessionId(HttpSessionCore session, String newId) {
        session.setId(newId);
    }

    @Override
    public void removeSession(String id) {
        collection.findOneAndDelete(new Document("id", id));
    }

    private class MongoSessionCore implements HttpSessionCore {

        private final ObjectId objectId;
        private final Bson filterByObjectId;
        private String id;
        private long creationTime;
        private long lastAccessedTime;
        private int maxInactiveInterval;
        private boolean valid;
        private Document attsDoc;

        public MongoSessionCore(Document doc) {
            this.objectId = doc.getObjectId("_id");
            this.filterByObjectId = Filters.eq("_id", objectId);
            id = doc.getString("id");
            creationTime = doc.getLong("creationTime");
            lastAccessedTime = doc.getLong("lastAccessedTime");
            maxInactiveInterval = doc.getInteger("maxInactiveInterval");
            valid = doc.getBoolean("valid");
            attsDoc = (Document) doc.get("atts");
        }

        @Override
        public String getId() {
            return id;
        }

        @Override
        public void setId(String id) {
            collection.updateOne(filterByObjectId, new Document("$set", new Document("id", id)));
            this.id = id;
        }

        @Override
        public long getCreationTime() {
            return creationTime;
        }

        @Override
        public long getLastAccessedTime() {
            return lastAccessedTime;
        }

        @Override
        public void setLastAccessedTime(long now) {
            lastAccessedTime = now;
            while (true) {
                Bson neFilter = Filters.and(filterByObjectId, Filters.ne("requestCount", 0));
                Document neUpdate = new Document("$set", new Document("lastAccessedTime", now).append("expiration", 0));
                Document doc = collection.findOneAndUpdate(neFilter, neUpdate);
                if (doc != null) return;
                doc = collection.find(filterByObjectId).first();
                if (doc == null) return; // already invalidated
                int interval = doc.getInteger("maxInactiveInterval");
                Bson eqFilter = Filters.and(filterByObjectId, Filters.eq("requestCount", 0), Filters.eq("maxInactiveInterval", interval));
                Object expDate = 0;
                if (maxInactiveInterval > 0) expDate = new Date(now + interval*1000L + EXPIRATION_LAG_MS);
                Document eqUpdate = new Document("$set", new Document("lastAccessedTime", now).append("expiration", expDate));
                doc = collection.findOneAndUpdate(eqFilter, eqUpdate);
                if (doc != null) {
                    maxInactiveInterval = interval;
                    return;
                }
            }
        }

        @Override
        public int getMaxInactiveInterval() {
            return maxInactiveInterval;
        }

        @Override
        public void setMaxInactiveInterval(int interval) {
            maxInactiveInterval = interval;
            while (true) {
                Bson neFilter = Filters.and(filterByObjectId, Filters.ne("requestCount", 0));
                Document neUpdate = new Document("$set", new Document("maxInactiveInterval", interval).append("expiration", 0));
                Document doc = collection.findOneAndUpdate(neFilter, neUpdate);
                if (doc != null) return;
                doc = collection.find(filterByObjectId).first();
                if (doc == null) return; // already invalidated
                long lastAccessed = doc.getLong("lastAccessedTime");
                Bson eqFilter = Filters.and(filterByObjectId, Filters.eq("requestCount", 0), Filters.eq("lastAccessedTime", lastAccessed));
                Object expDate = 0;
                if (interval > 0) expDate = new Date(lastAccessed + interval*1000L + EXPIRATION_LAG_MS);
                Document eqUpdate = new Document("$set", new Document("maxInactiveInterval", interval).append("expiration", expDate));
                doc = collection.findOneAndUpdate(eqFilter, eqUpdate);
                if (doc != null) {
                    lastAccessedTime = lastAccessed;
                    return;
                }
            }
        }

        @Override
        public long getExpiration() {
            return 0;
        }

        @Override
        public void setExpiration(long expiration) {
            // no-op
        }

        @Override
        public boolean canInvalidate() {
            return true;
        }

        @Override
        public boolean isValid() {
            return valid;
        }

        @Override
        public void setValid(boolean valid) {
            this.valid = valid;
            collection.updateOne(filterByObjectId, new Document("$set", new Document("valid", false)));
        }

        @Override
        public Object getAttribute(String name) {
            return deserialize(name, attsDoc.get(encodeKey(name)));
        }

        @Override
        public Collection<String> getAttributeNames() {
            return attsDoc.keySet().stream().map(MongoSessionCoreStore::decodeKey).collect(Collectors.toList());
        }

        @Override
        public Object setAttribute(String name, Object value) {
            Object storedValue = serialize(name, value);
            Object stored = attsDoc.put(encodeKey(name), storedValue);
            collection.updateOne(filterByObjectId, new Document("$set", new Document("atts." + encodeKey(name), storedValue)));
            if (stored == null) return null;
            return deserialize(name, stored);
        }

        @Override
        public Object removeAttribute(String name) {
            Object stored = attsDoc.remove(encodeKey(name));
            collection.updateOne(filterByObjectId, new Document("$unset", new Document("atts." + encodeKey(name), "")));
            if (stored == null) return null;
            return deserialize(name, stored);
        }

        @Override
        public void incrementRequestCount() {
            collection.updateOne(filterByObjectId, new Document("$inc", new Document("requestCount", 1)).append("$set", new Document("expiration", 0)));
        }

        @Override
        public int decrementAndGetRequestCount() {
            while (true) {
                Bson gtFilter = Filters.and(filterByObjectId, Filters.gt("requestCount", 1));
                Document gtUpdate = new Document("$inc", new Document("requestCount", -1));
                Document doc = collection.findOneAndUpdate(gtFilter, gtUpdate);
                if (doc != null) return doc.getInteger("requestCount");
                doc = collection.find(filterByObjectId).first();
                if (doc == null) return 0; // already invalidated
                long lastAccessed = doc.getLong("lastAccessedTime");
                int interval = doc.getInteger("maxInactiveInterval");
                Bson eqFilter = Filters.and(filterByObjectId, Filters.eq("requestCount", 1), Filters.eq("lastAccessedTime", lastAccessed), Filters.eq("maxInactiveInterval", interval));
                Object expDate = 0;
                if (maxInactiveInterval > 0) expDate = new Date(lastAccessed + interval*1000L + EXPIRATION_LAG_MS);
                Document eqUpdate = new Document("$set", new Document("requestCount", 0).append("expiration", expDate));
                doc = collection.findOneAndUpdate(eqFilter, eqUpdate);
                if (doc != null) {
                    lastAccessedTime = lastAccessed;
                    maxInactiveInterval = interval;
                    return 0;
                }
            }
        }

        @Override
        public int getRequestCount() {
            Document doc = collection.find(filterByObjectId).first();
            if (doc == null) return 0;
            return doc.getInteger("requestCount");
        }

    }

}
