package net.cnri.servletcontainer.sessions.jwt;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Map;
import java.util.stream.Stream;

import com.google.gson.Gson;

import net.cnri.servletcontainer.sessions.HttpSessionCore;
import net.cnri.servletcontainer.sessions.HttpSessionCoreStore;
import net.cnri.servletcontainer.sessions.HttpSessionManager;

public class PrototypeJwtHttpSessionCoreStore implements HttpSessionCoreStore {

    private long timeoutSeconds;

    @Override
    public void init(HttpSessionManager sessionManager, String sessionCoreStoreInit) {
        timeoutSeconds = sessionManager.getSessionTimeoutSeconds();
        // To move beyond the proof of concept, need to configure here with issuer and signing key
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void destroy() {
        // no-op
    }

    @Override
    public HttpSessionCore getSession(String id) {
        // proof of concept
        String json = new String(Base64.getUrlDecoder().decode(id), StandardCharsets.UTF_8);
        JwtHttpSessionCore sessionCore = new Gson().fromJson(json, JwtHttpSessionCore.class);
        return new JwtHttpSessionCore(id, sessionCore.iat, sessionCore.exp, sessionCore.atts);
    }

    @Override
    public HttpSessionCore createSession(Map<String, Object> attributes) {
        // proof of concept
        long iat = System.currentTimeMillis() / 1000L;
        long exp = iat + timeoutSeconds;
        JwtHttpSessionCore sessionCore = new JwtHttpSessionCore(null, iat, exp, attributes);
        String id = Base64.getUrlEncoder().encodeToString(new Gson().toJson(sessionCore).getBytes(StandardCharsets.UTF_8));
        return new JwtHttpSessionCore(id, sessionCore.iat, sessionCore.exp, sessionCore.atts);
    }

    @Override
    public void changeSessionId(HttpSessionCore session, String newId) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void removeSession(String id) {
        // ignore; called during cleanup
    }

    @Override
    public Stream<HttpSessionCore> getSessionsByKeyValue(String key, String value) {
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
