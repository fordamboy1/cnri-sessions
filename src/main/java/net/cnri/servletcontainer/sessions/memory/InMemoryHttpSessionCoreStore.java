package net.cnri.servletcontainer.sessions.memory;

import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import net.cnri.servletcontainer.sessions.HttpSessionCore;
import net.cnri.servletcontainer.sessions.HttpSessionCoreStore;
import net.cnri.servletcontainer.sessions.HttpSessionManager;

public class InMemoryHttpSessionCoreStore implements HttpSessionCoreStore {

    private final ConcurrentMap<String, HttpSessionCore> sessionsMap = new ConcurrentHashMap<>();
    private HttpSessionManager sessionManager;
    private ScheduledExecutorService executorService;

    @Override
    public void init(@SuppressWarnings("hiding") HttpSessionManager sessionManager, String sessionCoreStoreInit) {
        this.sessionManager = sessionManager;
        executorService = Executors.newSingleThreadScheduledExecutor();
        executorService.scheduleAtFixedRate(this::purge, 1, 1, TimeUnit.MINUTES);
    }

    @Override
    public void clear() {
        while (!sessionsMap.isEmpty()) {
            Iterator<HttpSessionCore> iter = sessionsMap.values().iterator();
            while (iter.hasNext()) {
                HttpSessionCore sessionCore = iter.next();
                sessionManager.ensureInvalid(sessionCore);
                iter.remove();
            }
        }
    }

    @Override
    public void destroy() {
        executorService.shutdown();
        try {
            executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        clear();
    }

    public void purge() {
        long now = System.currentTimeMillis();
        for (HttpSessionCore sessionCore : sessionsMap.values()) {
            sessionManager.ensureInvalidIfExpiredAndNoRequests(sessionCore, now);
        }
    }

    @Override
    public HttpSessionCore getSession(String id) {
        return sessionsMap.get(id);
    }

    @Override
    public HttpSessionCore createSession(Map<String, Object> attributes) {
        String id = sessionManager.mintNewSessionId();
        InMemoryHttpSessionCore res = new InMemoryHttpSessionCore(id, System.currentTimeMillis(), sessionManager.getSessionTimeoutSeconds());
        if (attributes != null) {
            res.setAllAttributes(attributes);
        }
        sessionsMap.put(id, res);
        return res;
    }

    @Override
    public void changeSessionId(HttpSessionCore session, String newId) {
        sessionsMap.remove(session.getId());
        session.setId(newId);
        sessionsMap.put(newId, session);
    }

    @Override
    public void removeSession(String id) {
        sessionsMap.remove(id);
    }

    @Override
    public Stream<HttpSessionCore> getSessionsByKeyValue(String key, String value) {
        if (key == null || value == null) return null;
        return sessionsMap.entrySet().stream()
                .filter(entry -> isKeyValueMatch(entry, key, value))
                .map(Map.Entry::getValue);
    }

    private boolean isKeyValueMatch(Map.Entry<String, HttpSessionCore> entry, String key, String value) {
        String attributeValue = (String) entry.getValue().getAttribute(key);
        return value.equals(attributeValue);
    }

}
