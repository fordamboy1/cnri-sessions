package net.cnri.servletcontainer.sessions.memory;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import net.cnri.servletcontainer.sessions.HttpSessionCore;

public class InMemoryHttpSessionCore implements HttpSessionCore {

    private volatile String id;
    private final long creationTime;
    private final AtomicLong lastAccessedTime;
    private volatile int maxInactiveInterval;
    private volatile boolean isValid;
    private final ConcurrentMap<String, Object> atts = new ConcurrentHashMap<>();
    private final AtomicInteger requestCount = new AtomicInteger();

    public InMemoryHttpSessionCore(String id, long creationTime, int maxInactiveInterval) {
        this.id = id;
        this.creationTime = creationTime;
        this.lastAccessedTime = new AtomicLong(creationTime);
        this.maxInactiveInterval = maxInactiveInterval;
        this.isValid = true;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public long getCreationTime() {
        return creationTime;
    }

    @Override
    public long getLastAccessedTime() {
        return lastAccessedTime.get();
    }

    @Override
    public void setLastAccessedTime(long now) {
        // set to now if now if greater than set time
        while (true) {
            long oldValue = lastAccessedTime.get();
            if (oldValue >= now) return;
            if (lastAccessedTime.compareAndSet(oldValue, now)) return;
        }
    }

    @Override
    public int getMaxInactiveInterval() {
        return maxInactiveInterval;
    }

    @Override
    public void setMaxInactiveInterval(int interval) {
        this.maxInactiveInterval = interval;
    }

    @Override
    public long getExpiration() {
        return 0;
    }

    @Override
    public void setExpiration(long expiration) {
        // no-op
    }

    @Override
    public boolean canInvalidate() {
        return true;
    }

    @Override
    public boolean isValid() {
        return isValid;
    }

    @Override
    public void setValid(boolean valid) {
        isValid = valid;
    }

    @Override
    public Object getAttribute(String name) {
        return atts.get(name);
    }

    @Override
    public Collection<String> getAttributeNames() {
        return atts.keySet();
    }

    @Override
    public Object setAttribute(String name, Object value) {
        return atts.put(name, value);
    }

    public void setAllAttributes(Map<String, Object> attributes) {
        this.atts.putAll(attributes);
    }

    @Override
    public Object removeAttribute(String name) {
        return atts.remove(name);
    }

    @Override
    public void incrementRequestCount() {
        requestCount.incrementAndGet();
    }

    @Override
    public int decrementAndGetRequestCount() {
        return requestCount.decrementAndGet();
    }

    @Override
    public int getRequestCount() {
        return requestCount.get();
    }
}
