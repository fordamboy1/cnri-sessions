package net.cnri.servletcontainer.sessions.functions;

import java.util.function.Function;

import javax.servlet.http.HttpServletRequest;

public class BearerTokenFunction implements Function<HttpServletRequest, String> {
    @Override
    public String apply(HttpServletRequest request) {
        String header = request.getHeader("Authorization");
        if (header == null) return null;
        String[] parts = header.trim().split(" +");
        if (parts.length != 2) return null;
        if (!"Bearer".equalsIgnoreCase(parts[0])) return null;
        return parts[1];
    }
}
