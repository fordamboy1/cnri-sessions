package net.cnri.servletcontainer.sessions;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import net.cnri.servletcontainer.sessions.functions.JSessionIdFunction;
import net.cnri.servletcontainer.sessions.memory.InMemoryHttpSessionCoreStore;

import java.io.IOException;
import java.util.function.Function;

public class HttpSessionManagerFilter implements Filter {

    private HttpSessionCoreStore sessionStore;
    private HttpSessionManager sessionManager;
    private Function<HttpServletRequest,String> requestedSessionIdFunction;

    @Override
    public void init(FilterConfig filterConfig) {
        ServletContext servletContext = filterConfig.getServletContext();
        String sessionCoreStoreClass = filterConfig.getInitParameter("session-core-store-class");
        String sessionTimeoutString = filterConfig.getInitParameter("session-timeout");
        String requestedSessionIdFunctionClass = filterConfig.getInitParameter("requested-session-id-function-class");
        int sessionTimeoutSeconds = HttpSessionManager.DEFAULT_SESSION_TIMEOUT_MINUTES * 60;
        if (sessionTimeoutString != null) {
            sessionTimeoutSeconds = Integer.parseInt(sessionTimeoutString) * 60;
        }
        String sessionCoreStoreInit = filterConfig.getInitParameter("session-core-store-init");
        try {
            init(servletContext, sessionCoreStoreClass, requestedSessionIdFunctionClass, sessionTimeoutSeconds, sessionCoreStoreInit);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void init(ServletContext servletContext, String sessionCoreStoreClass, String requestedSessionIdFunctionClass, int sessionTimeoutSeconds, String sessionCoreStoreInit)
        throws AssertionError, InstantiationException, IllegalAccessException, ClassNotFoundException {
        getSessionManager(servletContext);
        determineSessionStoreAndSetOnManager(sessionCoreStoreClass);
        sessionManager.setSessionTimeoutSeconds(sessionTimeoutSeconds);
        determineRequestedSessionIdFunction(requestedSessionIdFunctionClass);
        sessionStore.init(sessionManager, sessionCoreStoreInit);
    }

    private void getSessionManager(ServletContext servletContext) throws AssertionError {
        sessionManager = (HttpSessionManager) servletContext.getAttribute(HttpSessionManager.class.getName());
        if (sessionManager == null) throw new AssertionError("Need HttpSessionManagerListener");
    }

    private void determineSessionStoreAndSetOnManager(String sessionCoreStoreClass) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        if (sessionCoreStoreClass == null) sessionCoreStoreClass = InMemoryHttpSessionCoreStore.class.getName();
        sessionStore = (HttpSessionCoreStore) Class.forName(sessionCoreStoreClass).newInstance();
        sessionManager.setSessionStore(sessionStore);
    }

    @SuppressWarnings("unchecked")
    private void determineRequestedSessionIdFunction(String requestedSessionIdFunctionClass) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        if (requestedSessionIdFunctionClass == null) requestedSessionIdFunctionClass = JSessionIdFunction.class.getName();
        requestedSessionIdFunction = (Function<HttpServletRequest, String>) Class.forName(requestedSessionIdFunctionClass).newInstance();
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        // only wrap if not wrapped previously
        if (req instanceof SessionHandlingHttpServletRequestWrapper) {
            chain.doFilter(req, response);
        } else if (req instanceof HttpServletRequestWrapper && ((HttpServletRequestWrapper)req).isWrapperFor(SessionHandlingHttpServletRequestWrapper.class)) {
            chain.doFilter(req, response);
        } else {
            req = new SessionHandlingHttpServletRequestWrapper(req);
            chain.doFilter(req, response);
        }
    }

    @Override
    public void destroy() {
        sessionStore.destroy();
    }

    private class SessionHandlingHttpServletRequestWrapper extends HttpServletRequestWrapper {
        private final String requestedSessionId;
        private HttpSessionImpl cachedSession;

        private SessionHandlingHttpServletRequestWrapper(HttpServletRequest request) {
            super(request);
            requestedSessionId = requestedSessionIdFunction.apply(request);
        }

        @Override
        public HttpSessionImpl getSession() {
            return getSession(true);
        }

        @Override
        public HttpSessionImpl getSession(boolean create) {
            if (cachedSession != null && cachedSession.isValid()) return cachedSession;
            cachedSession = sessionManager.getSession(this, requestedSessionId, create);
            return cachedSession;
        }

        @Override
        public String getRequestedSessionId() {
            return requestedSessionId;
        }

        @Override
        public boolean isRequestedSessionIdValid() {
            if (requestedSessionId == null) return false;
            HttpSessionImpl session = getSession(false);
            return session != null && session.getId().equals(requestedSessionId);
        }

        @Override
        public boolean isRequestedSessionIdFromCookie() {
            return false;
        }

        @Override
        public boolean isRequestedSessionIdFromURL() {
            return false;
        }

        @Override
        public boolean isRequestedSessionIdFromUrl() {
            return isRequestedSessionIdFromURL();
        }

        @Override
        public String changeSessionId() {
            HttpSessionImpl session = getSession(false);
            if (session == null) {
                throw new IllegalStateException("No session");
            }
            String newId = sessionManager.mintNewSessionId();
            sessionStore.changeSessionId(this, session.getSessionCore(), newId);
            return session.getId();
        }
    }
}
