package net.cnri.servletcontainer.sessions;

import java.util.Collections;
import java.util.List;
import javax.servlet.*;

public class HttpSessionManagerListener implements ServletContextListener, ServletRequestListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContext servletContext = sce.getServletContext();
        turnOffContainerSessionTracking(servletContext);
        setupSessionManager(servletContext);
    }

    private void turnOffContainerSessionTracking(ServletContext servletContext) {
        servletContext.setSessionTrackingModes(Collections.emptySet());
    }

    private void setupSessionManager(ServletContext servletContext) {
        HttpSessionManager sessionManager = new HttpSessionManager(servletContext);
        servletContext.setAttribute(HttpSessionManager.class.getName(), sessionManager);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }

    @Override
    public void requestInitialized(ServletRequestEvent sre) {
    }

    @Override
    public void requestDestroyed(ServletRequestEvent sre) {
        @SuppressWarnings("unchecked")
        List<HttpSessionCore> sessions = (List<HttpSessionCore>) sre.getServletRequest().getAttribute(HttpSessionManager.COUNTED_SESSIONS_ATT);
        if (sessions != null) {
            for (HttpSessionCore sessionCore : sessions) {
                sessionCore.decrementAndGetRequestCount();
            }
        }

    }
}
