package net.cnri.servletcontainer.sessions.tomcat;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import net.cnri.servletcontainer.sessions.HttpSessionCore;
import net.cnri.servletcontainer.sessions.HttpSessionCoreStore;
import net.cnri.servletcontainer.sessions.HttpSessionManager;

public class TomcatSessionCoreStore implements HttpSessionCoreStore {

    private Object manager;
    private Method changeSessionIdMethod;
    private Method createSessionMethod;
    private Method findSessionMethod;
    private Method findSessionsMethod;
    private Field requestField;
    private Method setRequestedSessionIdMethod;

    private void initReflection(ServletContext servletContext) throws Exception {
        Class<?> applicationContextFacadeClass = Class.forName("org.apache.catalina.core.ApplicationContextFacade");
        Field contextField = applicationContextFacadeClass.getDeclaredField("context");
        contextField.setAccessible(true);
        Object applicationContext = contextField.get(servletContext);
        Class<?> applicationContextClass = Class.forName("org.apache.catalina.core.ApplicationContext");
        Method getContextMethod = applicationContextClass.getDeclaredMethod("getContext");
        getContextMethod.setAccessible(true);
        Object standardContext = getContextMethod.invoke(applicationContext);
        Class<?> contextClass = Class.forName("org.apache.catalina.Context");
        Method getManagerMethod = contextClass.getMethod("getManager");
        manager = getManagerMethod.invoke(standardContext);
        Class<?> managerClass = Class.forName("org.apache.catalina.Manager");
        Class<?> sessionClass = Class.forName("org.apache.catalina.Session");
        changeSessionIdMethod = managerClass.getMethod("changeSessionId", sessionClass, String.class);
        createSessionMethod = managerClass.getMethod("createSession", String.class);
        findSessionMethod = managerClass.getMethod("findSession", String.class);
        findSessionsMethod = managerClass.getMethod("findSessions");
        Class.forName(TomcatSessionCore.class.getName());
        Class<?> requestFacadeClass = Class.forName("org.apache.catalina.connector.RequestFacade");
        requestField = requestFacadeClass.getDeclaredField("request");
        requestField.setAccessible(true);
        Class<?> requestClass = Class.forName("org.apache.catalina.connector.Request");
        setRequestedSessionIdMethod = requestClass.getMethod("setRequestedSessionId", String.class);
    }

    private Object call(Method method, Object... params) {
        try {
            return method.invoke(manager, params);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new IllegalStateException("Unexpected failure to invoke reflective method", e);
        }
    }

    @Override
    public void init(HttpSessionManager sessionManager, String sessionCoreStoreInit) {
        ServletContext servletContext = sessionManager.getServletContext();
        if (!servletContext.getClass().getName().equals("org.apache.catalina.core.ApplicationContextFacade")) {
            throw new IllegalStateException("ServletContext is not the expected org.apache.catalina.core.ApplicationContextFacade; is this Tomcat?");
        }
        try {
            initReflection(servletContext);
        } catch (Exception e) {
            throw new IllegalStateException("Unable to get all expected Tomcat classes and methods via reflection", e);
        }
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void destroy() {
        // no-op
    }

    @Override
    public HttpSessionCore getSession(String id) {
        throw new AssertionError();
    }

    @Override
    public HttpSessionCore getSession(HttpServletRequest req, String id) {
        Object tomcatSession = call(findSessionMethod, id);
        if (tomcatSession == null) return null;
        setIdOnRequest(req, id);
        return new TomcatSessionCore(tomcatSession);
    }

    @Override
    public HttpSessionCore createSession(Map<String, Object> attributes) {
        throw new AssertionError();
    }

    @Override
    public HttpSessionCore createSession(HttpServletRequest req, Map<String, Object> attributes) {
        Object tomcatSession = call(createSessionMethod, (Object)null);
        HttpSessionCore res = new TomcatSessionCore(tomcatSession);
        if (attributes != null) {
            for (Map.Entry<String, Object> att : attributes.entrySet()) {
                res.setAttribute(att.getKey(), att.getValue());
            }
        }
        setIdOnRequest(req, res.getId());
        return res;
    }

    @Override
    public void changeSessionId(HttpSessionCore session, String newId) {
        throw new AssertionError();
    }

    @Override
    public void changeSessionId(HttpServletRequest req, HttpSessionCore session, String newId) {
        Object tomcatSession = ((TomcatSessionCore) session).getSession();
        call(changeSessionIdMethod, tomcatSession, newId);
        setIdOnRequest(req, newId);
    }

    private void setIdOnRequest(HttpServletRequest req, String id) {
        try {
            ServletRequest wrappedReq = req;
            while (wrappedReq instanceof HttpServletRequestWrapper) {
                wrappedReq = ((HttpServletRequestWrapper) wrappedReq).getRequest();
            }
            if (wrappedReq == null || !"org.apache.catalina.connector.RequestFacade".equals(wrappedReq.getClass().getName())) {
                req.getServletContext().log("Unexpected failure to find Tomcat RequestFacade");
                return;
            }
            Object request = requestField.get(wrappedReq);
            setRequestedSessionIdMethod.invoke(request, id);
        } catch (Exception e) {
            req.getServletContext().log("Unexpected error setting session id on Tomcat request", e);
        }
    }

    @Override
    public void removeSession(String id) {
        // no-op (let Tomcat deal with it)
    }

    @Override
    public Stream<HttpSessionCore> getSessionsByKeyValue(String key, String value) {
        List<HttpSessionCore> res = new ArrayList<>();
        Object array = call(findSessionsMethod);
        int length = Array.getLength(array);
        for (int i = 0; i < length; i++) {
            Object tomcatSession = Array.get(array, i);
            HttpSessionCore session = new TomcatSessionCore(tomcatSession);
            Object storedValue = session.getAttribute(key);
            if ((value == null && storedValue == null) || (value != null && value.equals(storedValue))) {
                res.add(session);
            }
        }
        return res.stream();
    }
}
