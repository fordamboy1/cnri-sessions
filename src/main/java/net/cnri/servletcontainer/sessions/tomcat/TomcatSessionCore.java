package net.cnri.servletcontainer.sessions.tomcat;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Collections;

import javax.servlet.http.HttpSession;

import net.cnri.servletcontainer.sessions.HttpSessionCore;

public class TomcatSessionCore implements HttpSessionCore {
    private static Method setId, isValid, setValid, expire, access, endAccess;

    private static void initReflection() throws Exception {
        Class<?> sessionClass = Class.forName("org.apache.catalina.Session");
        setId = sessionClass.getMethod("setId", String.class);
        isValid = sessionClass.getMethod("isValid");
        setValid = sessionClass.getMethod("setValid", Boolean.TYPE);
        expire = sessionClass.getMethod("expire");
        access = sessionClass.getMethod("access");
        endAccess = sessionClass.getMethod("endAccess");
    }

    static {
        try {
            initReflection();
        } catch (Exception e) {
            throw new RuntimeException("Unexpected reflection failure", e);
        }
    }

    private final HttpSession session;

    public TomcatSessionCore(Object session) {
        this.session = (HttpSession) session;
    }

    public Object getSession() {
        return session;
    }

    private Object call(Method method, Object... params) {
        try {
            return method.invoke(session, params);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new IllegalStateException("Unexpected failure to invoke reflective method", e);
        }
    }

    @Override
    public boolean handlesEvents() {
        return true;
    }

    @Override
    public String getId() {
        return session.getId();
    }

    @Override
    public void setId(String id) {
        call(setId, id);
    }

    @Override
    public long getCreationTime() {
        return session.getCreationTime();
    }

    @Override
    public long getLastAccessedTime() {
        return session.getLastAccessedTime();
    }

    @Override
    public void setLastAccessedTime(long now) {
        // no-op (handled by request count methods instead)
    }

    @Override
    public int getMaxInactiveInterval() {
        return session.getMaxInactiveInterval();
    }

    @Override
    public void setMaxInactiveInterval(int interval) {
        session.setMaxInactiveInterval(interval);
    }

    @Override
    public long getExpiration() {
        return 0;
    }

    @Override
    public void setExpiration(long expiration) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean canInvalidate() {
        return true;
    }

    @Override
    public boolean isValid() {
        return ((Boolean) call(isValid)).booleanValue();
    }

    @Override
    public void setValid(boolean valid) {
        if (!valid) {
            call(expire);
        }
        call(setValid, valid);
    }

    @Override
    public Object getAttribute(String name) {
        return session.getAttribute(name);
    }

    @Override
    public Collection<String> getAttributeNames() {
        return Collections.list(session.getAttributeNames());
    }

    @Override
    public Object setAttribute(String name, Object value) {
        session.setAttribute(name, value);
        return null; // leave notification to Tomcat
    }

    @Override
    public Object removeAttribute(String name) {
        session.removeAttribute(name);
        return null; // leave notification to Tomcat
    }

    @Override
    public void incrementRequestCount() {
        call(access);
    }

    @Override
    public int decrementAndGetRequestCount() {
        call(endAccess);
        return 1; // this means that we never run our expiration code; Tomcat's will take care of it instead
    }

    @Override
    public int getRequestCount() {
        return 1; // this means that we never run our expiration code; Tomcat's will take care of it instead
    }

}
