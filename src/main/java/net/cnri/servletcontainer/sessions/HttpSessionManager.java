package net.cnri.servletcontainer.sessions;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class HttpSessionManager {
    public static final String COUNTED_SESSIONS_ATT = "net.cnri.servletcontainer.sessions.countedSessions";
    public static final int DEFAULT_SESSION_TIMEOUT_MINUTES = 30;

    private final ServletContext servletContext;
    private final SecureRandom random = new SecureRandom();
    private HttpSessionCoreStore sessionStore;
    private int sessionTimeoutSeconds = DEFAULT_SESSION_TIMEOUT_MINUTES * 60;

    public HttpSessionManager(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    public ServletContext getServletContext() {
        return servletContext;
    }

    public HttpSessionCoreStore getSessionStore() {
        return sessionStore;
    }

    public void setSessionStore(HttpSessionCoreStore sessionStore) {
        this.sessionStore = sessionStore;
    }

    public int getSessionTimeoutSeconds() {
        return sessionTimeoutSeconds;
    }

    public void setSessionTimeoutSeconds(int sessionTimeoutSeconds) {
        this.sessionTimeoutSeconds = sessionTimeoutSeconds;
    }

    public void ensureInvalid(HttpSessionCore sessionCore) {
        if (sessionStore == null) throw new AssertionError("HttpSessionManager used before HttpSessionManagerFilter is initialized");
        sessionStore.removeSession(sessionCore.getId());
        HttpSessionImpl session = new HttpSessionImpl(this, sessionCore, false);
        session.ensureInvalid();
    }

    public void ensureInvalidIfExpiredAndNoRequests(HttpSessionCore sessionCore, long time) {
        if (HttpSessionCore.isExpired(sessionCore, time) && sessionCore.getRequestCount() == 0) {
            ensureInvalid(sessionCore);
        }
    }

    private boolean isStillUsable(HttpSessionCore sessionCore, boolean countRequest) {
        if (!sessionCore.isValid()) return false;
        long now = System.currentTimeMillis();
        if (countRequest) sessionCore.incrementRequestCount();
        if (HttpSessionCore.isExpired(sessionCore, now)) {
            if (countRequest) {
                if (sessionCore.decrementAndGetRequestCount() == 0) ensureInvalid(sessionCore);
            } else {
                if (sessionCore.getRequestCount() == 0) ensureInvalid(sessionCore);
            }
            return false;
        }
        sessionCore.setLastAccessedTime(now);
        return true;
    }

    private void markSessionAsCountedForReq(HttpSessionCore sessionCore, HttpServletRequest req) {
        @SuppressWarnings("unchecked")
        List<HttpSessionCore> countedSessions = (List<HttpSessionCore>) req.getAttribute(COUNTED_SESSIONS_ATT);
        if (countedSessions == null) {
            countedSessions = new ArrayList<>();
            req.setAttribute(COUNTED_SESSIONS_ATT, countedSessions);
        }
        countedSessions.add(sessionCore);
    }

    public HttpSessionImpl getSession(HttpServletRequest req, String requestedSessionId, boolean create) {
        return getSession(req, requestedSessionId, create, null);
    }

    public HttpSessionImpl getSession(HttpServletRequest req, String requestedSessionId, boolean create, Map<String, Object> attributes) {
        if (sessionStore == null) throw new AssertionError("HttpSessionManager used before HttpSessionManagerFilter is initialized");
        if (requestedSessionId != null) {
            HttpSessionCore sessionCore = sessionStore.getSession(req, requestedSessionId);
            if (sessionCore != null) {
                if (isStillUsable(sessionCore, req != null)) {
                    if (req != null) markSessionAsCountedForReq(sessionCore, req);
                    return new HttpSessionImpl(this, sessionCore, false);
                }
            }
        }
        if (!create) {
            return null;
        }
        HttpSessionCore sessionCore = sessionStore.createSession(req, attributes);
        if (req != null) {
            sessionCore.incrementRequestCount();
            markSessionAsCountedForReq(sessionCore, req);
        }
        return new HttpSessionImpl(this, sessionCore, true);
    }

    public Stream<HttpSessionCore> getSessionsByKeyValue(String key, String value) {
        if (sessionStore == null) throw new AssertionError("HttpSessionManager used before HttpSessionManagerFilter is initialized");
        return sessionStore.getSessionsByKeyValue(key, value);
    }

    public String mintNewSessionId() {
        long a = random.nextLong();
        if (a < 0) a = -a;
        long b = random.nextLong();
        if (b < 0) b = -b;
        return Long.toString(a, 36) + Long.toString(b, 36);
    }
}
